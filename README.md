# FFMpeg updating script

This script is to update your FFmpeg static builds from
`https://johnvansickle.com/ffmpeg/`.

First it is validating if this script is running on the latest version
or it should be updated.
Next it is testing if there is a new release of ffmpeg-git-amd64-static.tar.xz
by using the md5 sum values

You following 5 lines should be adjusted to your need, but they are already
put to a standard 64bit debian like linux and should match most peoples needs

------

This bash script is to install and keep ffmpeg static build up to date
from [FFmpeg Static Builds](https://johnvansickle.com/ffmpeg/ "FFmpeg
Static Builds").

To use this script you will as root (sudo) download script to
`/usr/local/bin/ffmpeg-update` and set the executable bit
`sudo chmod +x /usr/local/bin/ffmpeg-update`

**NEXT AND MOST IMPORTANT**
Configure the following parameters to match your personal needs.

```bash
download_dir="/opt/ffmpeg-static"
dest_dir="/usr/bin/"
version="ffmpeg-git-amd64-static.tar.xz"
```

# Install
To install this script, you simply run the `ffmpeg_update.bash`

This can be done straight into bash by piping it with `curl`

```shell
bash <(curl -sSL https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds/-/raw/master/ffmpeg_update.bash)
```

or by using `wget`

```shell
wget -qO - https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds/-/raw/master/ffmpeg_update.bash | bash -
```

# Uninstall
To uninstall this script, you have to remove it from the `/etc/cron.daily/`,
and since all files in linux is just a file, then it is as simple as removing
the cron file from `/etc/cron.daily/`

```shell
sudo rm -f /etc/cron.daily/ffmpeg_update_static_build
```

-----

# Future
In the future we should enhance the script to pick the right version for
downloading to any given non proprietarian *AND* privacy aware OS

# Formalities
## Copyright
@spirillen https://mypdns.org/spirillen

## License
GNU LESSER GENERAL PUBLIC LICENSE:
https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds/-/master/License

This license superseeds the [mypdns general license][My Privacy DNS LICENSE]

## Issues
https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds/-/issues

## Project link
https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds

[My Privacy DNS LICENSE]: https://mypdns.org/mypdns/support/-/wikis/License "My Privacy DNS LICENSE"
