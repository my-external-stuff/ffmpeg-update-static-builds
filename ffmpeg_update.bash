#!/usr/bin/env bash

# All Copyright: @spirillen https://mypdns.org/spirillen
# License: https://mypdns.org/mypdns/support/-/wikis/License
# Issues: https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds/-/issues
# Project link: https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds

# This script is to update your FFmpeg static builds from
# https://johnvansickle.com/ffmpeg/.

# First it is validating if this script is running on the latest version
# or it should be updated.
# Next it is testing if there is a new release of ffmpeg-git-amd64-static.tar.xz
# by using the md5 sum values

# You following 5 lines should be adjusted to your need, but they are already
# put to a standard 64bit debian like linux and should match most peoples needs

DOWNLOAD_DIR="/opt/ffmpeg-static"
DEST_DIR="/usr/bin/"
VERSION="ffmpeg-git-amd64-static.tar.xz"
AUTOUPDATE_SCRIPT="1"
SCRIPT_SOURCE="https://mypdns.org/my-external-stuff/ffmpeg-update-static-builds/-/raw/master/ffmpeg_update.bash" # Source of this script
SCRIPT_MD5="${SCRIPT_SOURCE}.md5"

#################### Do not touch anything below here ##########################

FFMPEG_URL="https://johnvansickle.com/ffmpeg/builds/${VERSION}"
MD5_URL="${FFMPEG_URL}.md5"

SCRIPT_DIR=$(DIRNAME=$(dirname "$0"); cd "$DIRNAME"; pwd)

# Make sure we always exit on any error to avoid breaking anything as we
# are running with root privileges, we can seriously fuck up systems
set -e


# Make sure requirements are meet
pkgs="curl tar"
if ! dpkg -s $pkgs >/dev/null 2>&1; then
  sudo apt-get install $pkgs
fi


function install_ffmpeg () {
    rm -f ${VERSION} # Remove old files
    
    # Download new package
    curl ${FFMPEG_URL} -o ${DOWNLOAD_DIR}/${VERSION} --retry 5 --retry-delay 2

    # Unpack FFmpeg
    tar -xf ffmpeg-git-*-static.tar.xz

    # Moved unpacked files to the right location    
    mv ${DOWNLOAD_DIR}/ffmpeg-*-static/ff* "${DEST_DIR}"
    mv ${DOWNLOAD_DIR}/ffmpeg-*-static/qt-faststart "${DEST_DIR}"

    ln -sfn ${DEST_DIR}/qt-faststart /usr/local/bin/qt-faststart
    ln -sfn ${DEST_DIR}/ffmpeg /usr/local/bin/ffmpeg
    # ln -sfn ${DEST_DIR}/ffmpeg-10bit /usr/local/bin/ffmpeg-10bit
    ln -sfn ${DEST_DIR}/ffprobe /usr/local/bin/ffprobe
    # ln -sfn ${DEST_DIR}/ffserver /usr/local/bin/ffserver

    rm -fr ffmpeg-git-*-static

    update_cron
}

function update_cron () {
    # https://stackoverflow.com/a/42774010
    # https://stackoverflow.com/a/52992588
    if [ ! -f "/etc/cron.daily/ffmpeg_update_static_build" ]
    then
        # sudo echo "2 55 * * * root $SCRIPT_DIR/${0} 2>&1" >> "/etc/cron.d/ffmpeg_update_static_build"
        sudo cp -f "$SCRIPT_DIR/ffmpeg_update_static_build.cron" "/etc/cron.daily/ffmpeg_update_static_build"
    fi
}

if [ "$AUTOUPDATE_SCRIPT" == '1' ]

echo ""
echo "Checking for new releases of this script"
echo ""

then
    if ! diff <(md5sum "${0}") <(curl -s ${SCRIPT_MD5})
    then
        function script_update () {
            curl "${SCRIPT_SOURCE}" -O "${0}" --retry 5 --retry-delay 2
            if [ -f "${0}" ]
            then
                chmod +x "${0}" && exec bash "${0}" 
                echo "updated ${0}"
            else
                echo "Something went wrong updating this script"
            fi
        }
    else
        echo "You are already using latest version of the script"
    fi
else
    echo -e "\nYou might be running on a old script."
    echo "Please consider checking for updated version of this script"
    echo ""
fi


if [ $(id -u) -eq 0 ]
then
    # Download FFMPEG static daily build and it's md5
    mkdir -p "${DEST_DIR}" "${DOWNLOAD_DIR}"
    cd ${DOWNLOAD_DIR} || {
    echo "You can't enter this folder."
    echo "Please change DOWNLOAD_DIR in this script..."
    echo ""
    exit 1
}


if ! diff <(md5sum ${VERSION}) <(curl -s ${MD5_URL})
then
    # Sum doesn't match is not really an error... I comment out the
    # redirection.
    echo "md5sum doesn't match..." "Downloading new version"
    echo ""
    
    install_ffmpeg
else
    # You've done this already.
    echo "Nothing new to download"
fi

    exit 0
else
    # Multiple tricks to run this script as root before running the real script
    echo "This script needs to be ran as root, let's switch to root:" > /dev/stderr
    sudo "$0" && exit 0
    if [ $? -eq 127 ] ; then
        echo "I can't find 'sudo', I'll try to use 'su' to become root." > /dev/stderr
        echo "(Remember that 'su' is considered deprecated (it needs a root password), I strongly recommend installing 'sudo'.)" > /dev/stderr
        su -c "$0" && exit 0
        if [ $? -eq 127 ] ; then
            echo "I also can't find 'su', I'll try to use 'pkexec' to run this as root..." > /dev/stderr
            pkexec "$0" && exit 0
            #Check for 126 because 127 is a possible returncode of 'pkexec' so it's not necessarily unavailable
            #Because of the '&&' in the command above checks for returncode 0, the else-block can only be entered in case of non-authorization-related problems
            if [ $? -eq 126 ] ; then
                echo "I can't give you authorization to run this as root, you'll need to find another way to become root..." > /dev/stderr
                exit 1
            else
                echo "Something went wrong with trying to use 'pkexec' to run this script." > /dev/stderr
                echo "It could also be that it's not installed in which case you need to install at least 'sudo', 'pkexec' or 'su'" > /dev/stderr
                exit 1
            fi
        else
            echo "Something went wrong with trying to use 'su' to run this script" > /dev/stderr
            exit 1
        fi
    else
        echo "Something went wrong with trying to use 'sudo' to run this script" > /dev/stderr
        exit 1
    fi
fi
